const name = 'nadia-maginifier'
const extension = require('extensionizer')
const manifest = extension.runtime.getManifest()

crx3([`dist/manifest.json`], {
  keyPath: `dist/key.pem`,
  crxPath: `dist/${name}.crx`,
  zipPath: `dist/${name}.zip`,
  xmlPath: `dist/update.xml`,
  crxURL: `http://ext.myppl.pl/${name}.crx`,
  appVersion: process.env.EXT_VERSION
})
  .then(() => console.log('done'))
  .catch(err => {
    console.log(err)
  })
