(function(modules) {
  var installedModules = {};
  function __webpack_require__(moduleId) {
    if (installedModules[moduleId]) {
      return installedModules[moduleId].exports;
    }
    var module = installedModules[moduleId] = {
      i: moduleId,
      l: false,
      exports: {}
    };
    modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
    module.l = true;
    return module.exports;
  }
  __webpack_require__.m = modules;
  __webpack_require__.c = installedModules;
  __webpack_require__.d = function(exports, name, getter) {
    if (!__webpack_require__.o(exports, name)) {
      Object.defineProperty(exports, name, {
        enumerable: true,
        get: getter
      });
    }
  };
  __webpack_require__.r = function(exports) {
    if (typeof Symbol !== "undefined" && Symbol.toStringTag) {
      Object.defineProperty(exports, Symbol.toStringTag, {
        value: "Module"
      });
    }
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
  };
  __webpack_require__.t = function(value, mode) {
    if (mode & 1) value = __webpack_require__(value);
    if (mode & 8) return value;
    if (mode & 4 && typeof value === "object" && value && value.__esModule) return value;
    var ns = Object.create(null);
    __webpack_require__.r(ns);
    Object.defineProperty(ns, "default", {
      enumerable: true,
      value: value
    });
    if (mode & 2 && typeof value != "string") for (var key in value) __webpack_require__.d(ns, key, function(key) {
      return value[key];
    }.bind(null, key));
    return ns;
  };
  __webpack_require__.n = function(module) {
    var getter = module && module.__esModule ? function getDefault() {
      return module["default"];
    } : function getModuleExports() {
      return module;
    };
    __webpack_require__.d(getter, "a", getter);
    return getter;
  };
  __webpack_require__.o = function(object, property) {
    return Object.prototype.hasOwnProperty.call(object, property);
  };
  __webpack_require__.p = "";
  return __webpack_require__(__webpack_require__.s = 562);
})({
  14: function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    var webext_options_sync__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(30);
    var webext_options_sync__WEBPACK_IMPORTED_MODULE_0___default = __webpack_require__.n(webext_options_sync__WEBPACK_IMPORTED_MODULE_0__);
    const storage = new webext_options_sync__WEBPACK_IMPORTED_MODULE_0___default.a();
    __webpack_exports__["a"] = storage;
  },
  146: function(module, exports) {
    var process = module.exports = {};
    var cachedSetTimeout;
    var cachedClearTimeout;
    function defaultSetTimout() {
      throw new Error("setTimeout has not been defined");
    }
    function defaultClearTimeout() {
      throw new Error("clearTimeout has not been defined");
    }
    (function() {
      try {
        if (typeof setTimeout === "function") {
          cachedSetTimeout = setTimeout;
        } else {
          cachedSetTimeout = defaultSetTimout;
        }
      } catch (e) {
        cachedSetTimeout = defaultSetTimout;
      }
      try {
        if (typeof clearTimeout === "function") {
          cachedClearTimeout = clearTimeout;
        } else {
          cachedClearTimeout = defaultClearTimeout;
        }
      } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
      }
    })();
    function runTimeout(fun) {
      if (cachedSetTimeout === setTimeout) {
        return setTimeout(fun, 0);
      }
      if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
      }
      try {
        return cachedSetTimeout(fun, 0);
      } catch (e) {
        try {
          return cachedSetTimeout.call(null, fun, 0);
        } catch (e) {
          return cachedSetTimeout.call(this, fun, 0);
        }
      }
    }
    function runClearTimeout(marker) {
      if (cachedClearTimeout === clearTimeout) {
        return clearTimeout(marker);
      }
      if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
      }
      try {
        return cachedClearTimeout(marker);
      } catch (e) {
        try {
          return cachedClearTimeout.call(null, marker);
        } catch (e) {
          return cachedClearTimeout.call(this, marker);
        }
      }
    }
    var queue = [];
    var draining = false;
    var currentQueue;
    var queueIndex = -1;
    function cleanUpNextTick() {
      if (!draining || !currentQueue) {
        return;
      }
      draining = false;
      if (currentQueue.length) {
        queue = currentQueue.concat(queue);
      } else {
        queueIndex = -1;
      }
      if (queue.length) {
        drainQueue();
      }
    }
    function drainQueue() {
      if (draining) {
        return;
      }
      var timeout = runTimeout(cleanUpNextTick);
      draining = true;
      var len = queue.length;
      while (len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
          if (currentQueue) {
            currentQueue[queueIndex].run();
          }
        }
        queueIndex = -1;
        len = queue.length;
      }
      currentQueue = null;
      draining = false;
      runClearTimeout(timeout);
    }
    process.nextTick = function(fun) {
      var args = new Array(arguments.length - 1);
      if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
          args[i - 1] = arguments[i];
        }
      }
      queue.push(new Item(fun, args));
      if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
      }
    };
    function Item(fun, array) {
      this.fun = fun;
      this.array = array;
    }
    Item.prototype.run = function() {
      this.fun.apply(null, this.array);
    };
    process.title = "browser";
    process.browser = true;
    process.env = {};
    process.argv = [];
    process.version = "";
    process.versions = {};
    function noop() {}
    process.on = noop;
    process.addListener = noop;
    process.once = noop;
    process.off = noop;
    process.removeListener = noop;
    process.removeAllListeners = noop;
    process.emit = noop;
    process.prependListener = noop;
    process.prependOnceListener = noop;
    process.listeners = function(name) {
      return [];
    };
    process.binding = function(name) {
      throw new Error("process.binding is not supported");
    };
    process.cwd = function() {
      return "/";
    };
    process.chdir = function(dir) {
      throw new Error("process.chdir is not supported");
    };
    process.umask = function() {
      return 0;
    };
  },
  19: function(module, exports, __webpack_require__) {
    var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;
    (function(global, factory) {
      if (true) {
        !(__WEBPACK_AMD_DEFINE_ARRAY__ = [ module ], __WEBPACK_AMD_DEFINE_FACTORY__ = factory, 
        __WEBPACK_AMD_DEFINE_RESULT__ = typeof __WEBPACK_AMD_DEFINE_FACTORY__ === "function" ? __WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__) : __WEBPACK_AMD_DEFINE_FACTORY__, 
        __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
      } else {
        var mod;
      }
    })(this, function(module) {
      "use strict";
      if (typeof browser === "undefined") {
        const wrapAPIs = () => {
          const apiMetadata = {
            alarms: {
              clear: {
                minArgs: 0,
                maxArgs: 1
              },
              clearAll: {
                minArgs: 0,
                maxArgs: 0
              },
              get: {
                minArgs: 0,
                maxArgs: 1
              },
              getAll: {
                minArgs: 0,
                maxArgs: 0
              }
            },
            bookmarks: {
              create: {
                minArgs: 1,
                maxArgs: 1
              },
              export: {
                minArgs: 0,
                maxArgs: 0
              },
              get: {
                minArgs: 1,
                maxArgs: 1
              },
              getChildren: {
                minArgs: 1,
                maxArgs: 1
              },
              getRecent: {
                minArgs: 1,
                maxArgs: 1
              },
              getTree: {
                minArgs: 0,
                maxArgs: 0
              },
              getSubTree: {
                minArgs: 1,
                maxArgs: 1
              },
              import: {
                minArgs: 0,
                maxArgs: 0
              },
              move: {
                minArgs: 2,
                maxArgs: 2
              },
              remove: {
                minArgs: 1,
                maxArgs: 1
              },
              removeTree: {
                minArgs: 1,
                maxArgs: 1
              },
              search: {
                minArgs: 1,
                maxArgs: 1
              },
              update: {
                minArgs: 2,
                maxArgs: 2
              }
            },
            browserAction: {
              getBadgeBackgroundColor: {
                minArgs: 1,
                maxArgs: 1
              },
              getBadgeText: {
                minArgs: 1,
                maxArgs: 1
              },
              getPopup: {
                minArgs: 1,
                maxArgs: 1
              },
              getTitle: {
                minArgs: 1,
                maxArgs: 1
              },
              setIcon: {
                minArgs: 1,
                maxArgs: 1
              }
            },
            commands: {
              getAll: {
                minArgs: 0,
                maxArgs: 0
              }
            },
            contextMenus: {
              update: {
                minArgs: 2,
                maxArgs: 2
              },
              remove: {
                minArgs: 1,
                maxArgs: 1
              },
              removeAll: {
                minArgs: 0,
                maxArgs: 0
              }
            },
            cookies: {
              get: {
                minArgs: 1,
                maxArgs: 1
              },
              getAll: {
                minArgs: 1,
                maxArgs: 1
              },
              getAllCookieStores: {
                minArgs: 0,
                maxArgs: 0
              },
              remove: {
                minArgs: 1,
                maxArgs: 1
              },
              set: {
                minArgs: 1,
                maxArgs: 1
              }
            },
            devtools: {
              inspectedWindow: {
                eval: {
                  minArgs: 1,
                  maxArgs: 2
                }
              },
              panels: {
                create: {
                  minArgs: 3,
                  maxArgs: 3,
                  singleCallbackArg: true
                }
              }
            },
            downloads: {
              download: {
                minArgs: 1,
                maxArgs: 1
              },
              cancel: {
                minArgs: 1,
                maxArgs: 1
              },
              erase: {
                minArgs: 1,
                maxArgs: 1
              },
              getFileIcon: {
                minArgs: 1,
                maxArgs: 2
              },
              open: {
                minArgs: 1,
                maxArgs: 1
              },
              pause: {
                minArgs: 1,
                maxArgs: 1
              },
              removeFile: {
                minArgs: 1,
                maxArgs: 1
              },
              resume: {
                minArgs: 1,
                maxArgs: 1
              },
              search: {
                minArgs: 1,
                maxArgs: 1
              },
              show: {
                minArgs: 1,
                maxArgs: 1
              }
            },
            extension: {
              isAllowedFileSchemeAccess: {
                minArgs: 0,
                maxArgs: 0
              },
              isAllowedIncognitoAccess: {
                minArgs: 0,
                maxArgs: 0
              }
            },
            history: {
              addUrl: {
                minArgs: 1,
                maxArgs: 1
              },
              getVisits: {
                minArgs: 1,
                maxArgs: 1
              },
              deleteAll: {
                minArgs: 0,
                maxArgs: 0
              },
              deleteRange: {
                minArgs: 1,
                maxArgs: 1
              },
              deleteUrl: {
                minArgs: 1,
                maxArgs: 1
              },
              search: {
                minArgs: 1,
                maxArgs: 1
              }
            },
            i18n: {
              detectLanguage: {
                minArgs: 1,
                maxArgs: 1
              },
              getAcceptLanguages: {
                minArgs: 0,
                maxArgs: 0
              }
            },
            idle: {
              queryState: {
                minArgs: 1,
                maxArgs: 1
              }
            },
            management: {
              get: {
                minArgs: 1,
                maxArgs: 1
              },
              getAll: {
                minArgs: 0,
                maxArgs: 0
              },
              getSelf: {
                minArgs: 0,
                maxArgs: 0
              },
              uninstallSelf: {
                minArgs: 0,
                maxArgs: 1
              }
            },
            notifications: {
              clear: {
                minArgs: 1,
                maxArgs: 1
              },
              create: {
                minArgs: 1,
                maxArgs: 2
              },
              getAll: {
                minArgs: 0,
                maxArgs: 0
              },
              getPermissionLevel: {
                minArgs: 0,
                maxArgs: 0
              },
              update: {
                minArgs: 2,
                maxArgs: 2
              }
            },
            pageAction: {
              getPopup: {
                minArgs: 1,
                maxArgs: 1
              },
              getTitle: {
                minArgs: 1,
                maxArgs: 1
              },
              hide: {
                minArgs: 0,
                maxArgs: 0
              },
              setIcon: {
                minArgs: 1,
                maxArgs: 1
              },
              show: {
                minArgs: 0,
                maxArgs: 0
              }
            },
            runtime: {
              getBackgroundPage: {
                minArgs: 0,
                maxArgs: 0
              },
              getBrowserInfo: {
                minArgs: 0,
                maxArgs: 0
              },
              getPlatformInfo: {
                minArgs: 0,
                maxArgs: 0
              },
              openOptionsPage: {
                minArgs: 0,
                maxArgs: 0
              },
              requestUpdateCheck: {
                minArgs: 0,
                maxArgs: 0
              },
              sendMessage: {
                minArgs: 1,
                maxArgs: 3
              },
              sendNativeMessage: {
                minArgs: 2,
                maxArgs: 2
              },
              setUninstallURL: {
                minArgs: 1,
                maxArgs: 1
              }
            },
            storage: {
              local: {
                clear: {
                  minArgs: 0,
                  maxArgs: 0
                },
                get: {
                  minArgs: 0,
                  maxArgs: 1
                },
                getBytesInUse: {
                  minArgs: 0,
                  maxArgs: 1
                },
                remove: {
                  minArgs: 1,
                  maxArgs: 1
                },
                set: {
                  minArgs: 1,
                  maxArgs: 1
                }
              },
              managed: {
                get: {
                  minArgs: 0,
                  maxArgs: 1
                },
                getBytesInUse: {
                  minArgs: 0,
                  maxArgs: 1
                }
              },
              sync: {
                clear: {
                  minArgs: 0,
                  maxArgs: 0
                },
                get: {
                  minArgs: 0,
                  maxArgs: 1
                },
                getBytesInUse: {
                  minArgs: 0,
                  maxArgs: 1
                },
                remove: {
                  minArgs: 1,
                  maxArgs: 1
                },
                set: {
                  minArgs: 1,
                  maxArgs: 1
                }
              }
            },
            tabs: {
              create: {
                minArgs: 1,
                maxArgs: 1
              },
              captureVisibleTab: {
                minArgs: 0,
                maxArgs: 2
              },
              detectLanguage: {
                minArgs: 0,
                maxArgs: 1
              },
              duplicate: {
                minArgs: 1,
                maxArgs: 1
              },
              executeScript: {
                minArgs: 1,
                maxArgs: 2
              },
              get: {
                minArgs: 1,
                maxArgs: 1
              },
              getCurrent: {
                minArgs: 0,
                maxArgs: 0
              },
              getZoom: {
                minArgs: 0,
                maxArgs: 1
              },
              getZoomSettings: {
                minArgs: 0,
                maxArgs: 1
              },
              highlight: {
                minArgs: 1,
                maxArgs: 1
              },
              insertCSS: {
                minArgs: 1,
                maxArgs: 2
              },
              move: {
                minArgs: 2,
                maxArgs: 2
              },
              reload: {
                minArgs: 0,
                maxArgs: 2
              },
              remove: {
                minArgs: 1,
                maxArgs: 1
              },
              query: {
                minArgs: 1,
                maxArgs: 1
              },
              removeCSS: {
                minArgs: 1,
                maxArgs: 2
              },
              sendMessage: {
                minArgs: 2,
                maxArgs: 3
              },
              setZoom: {
                minArgs: 1,
                maxArgs: 2
              },
              setZoomSettings: {
                minArgs: 1,
                maxArgs: 2
              },
              update: {
                minArgs: 1,
                maxArgs: 2
              }
            },
            webNavigation: {
              getAllFrames: {
                minArgs: 1,
                maxArgs: 1
              },
              getFrame: {
                minArgs: 1,
                maxArgs: 1
              }
            },
            webRequest: {
              handlerBehaviorChanged: {
                minArgs: 0,
                maxArgs: 0
              }
            },
            windows: {
              create: {
                minArgs: 0,
                maxArgs: 1
              },
              get: {
                minArgs: 1,
                maxArgs: 2
              },
              getAll: {
                minArgs: 0,
                maxArgs: 1
              },
              getCurrent: {
                minArgs: 0,
                maxArgs: 1
              },
              getLastFocused: {
                minArgs: 0,
                maxArgs: 1
              },
              remove: {
                minArgs: 1,
                maxArgs: 1
              },
              update: {
                minArgs: 2,
                maxArgs: 2
              }
            }
          };
          if (Object.keys(apiMetadata).length === 0) {
            throw new Error("api-metadata.json has not been included in browser-polyfill");
          }
          class DefaultWeakMap extends WeakMap {
            constructor(createItem, items = undefined) {
              super(items);
              this.createItem = createItem;
            }
            get(key) {
              if (!this.has(key)) {
                this.set(key, this.createItem(key));
              }
              return super.get(key);
            }
          }
          const isThenable = value => {
            return value && typeof value === "object" && typeof value.then === "function";
          };
          const makeCallback = (promise, metadata) => {
            return (...callbackArgs) => {
              if (chrome.runtime.lastError) {
                promise.reject(chrome.runtime.lastError);
              } else if (metadata.singleCallbackArg || callbackArgs.length === 1) {
                promise.resolve(callbackArgs[0]);
              } else {
                promise.resolve(callbackArgs);
              }
            };
          };
          const wrapAsyncFunction = (name, metadata) => {
            const pluralizeArguments = numArgs => numArgs == 1 ? "argument" : "arguments";
            return function asyncFunctionWrapper(target, ...args) {
              if (args.length < metadata.minArgs) {
                throw new Error(`Expected at least ${metadata.minArgs} ${pluralizeArguments(metadata.minArgs)} for ${name}(), got ${args.length}`);
              }
              if (args.length > metadata.maxArgs) {
                throw new Error(`Expected at most ${metadata.maxArgs} ${pluralizeArguments(metadata.maxArgs)} for ${name}(), got ${args.length}`);
              }
              return new Promise((resolve, reject) => {
                target[name](...args, makeCallback({
                  resolve: resolve,
                  reject: reject
                }, metadata));
              });
            };
          };
          const wrapMethod = (target, method, wrapper) => {
            return new Proxy(method, {
              apply(targetMethod, thisObj, args) {
                return wrapper.call(thisObj, target, ...args);
              }
            });
          };
          let hasOwnProperty = Function.call.bind(Object.prototype.hasOwnProperty);
          const wrapObject = (target, wrappers = {}, metadata = {}) => {
            let cache = Object.create(null);
            let handlers = {
              has(target, prop) {
                return prop in target || prop in cache;
              },
              get(target, prop, receiver) {
                if (prop in cache) {
                  return cache[prop];
                }
                if (!(prop in target)) {
                  return undefined;
                }
                let value = target[prop];
                if (typeof value === "function") {
                  if (typeof wrappers[prop] === "function") {
                    value = wrapMethod(target, target[prop], wrappers[prop]);
                  } else if (hasOwnProperty(metadata, prop)) {
                    let wrapper = wrapAsyncFunction(prop, metadata[prop]);
                    value = wrapMethod(target, target[prop], wrapper);
                  } else {
                    value = value.bind(target);
                  }
                } else if (typeof value === "object" && value !== null && (hasOwnProperty(wrappers, prop) || hasOwnProperty(metadata, prop))) {
                  value = wrapObject(value, wrappers[prop], metadata[prop]);
                } else {
                  Object.defineProperty(cache, prop, {
                    configurable: true,
                    enumerable: true,
                    get() {
                      return target[prop];
                    },
                    set(value) {
                      target[prop] = value;
                    }
                  });
                  return value;
                }
                cache[prop] = value;
                return value;
              },
              set(target, prop, value, receiver) {
                if (prop in cache) {
                  cache[prop] = value;
                } else {
                  target[prop] = value;
                }
                return true;
              },
              defineProperty(target, prop, desc) {
                return Reflect.defineProperty(cache, prop, desc);
              },
              deleteProperty(target, prop) {
                return Reflect.deleteProperty(cache, prop);
              }
            };
            return new Proxy(target, handlers);
          };
          const wrapEvent = wrapperMap => ({
            addListener(target, listener, ...args) {
              target.addListener(wrapperMap.get(listener), ...args);
            },
            hasListener(target, listener) {
              return target.hasListener(wrapperMap.get(listener));
            },
            removeListener(target, listener) {
              target.removeListener(wrapperMap.get(listener));
            }
          });
          const onMessageWrappers = new DefaultWeakMap(listener => {
            if (typeof listener !== "function") {
              return listener;
            }
            return function onMessage(message, sender, sendResponse) {
              let result = listener(message, sender);
              if (isThenable(result)) {
                result.then(sendResponse, error => {
                  console.error(error);
                  sendResponse(error);
                });
                return true;
              } else if (result !== undefined) {
                sendResponse(result);
              }
            };
          });
          const staticWrappers = {
            runtime: {
              onMessage: wrapEvent(onMessageWrappers)
            }
          };
          const targetObject = Object.assign({}, chrome);
          return wrapObject(targetObject, staticWrappers, apiMetadata);
        };
        module.exports = wrapAPIs();
      } else {
        module.exports = browser;
      }
    });
  },
  24: function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.d(__webpack_exports__, "d", function() {
      return UPDATE_NOTIFICATION_TYPES;
    });
    __webpack_require__.d(__webpack_exports__, "b", function() {
      return MATCH_ROOM_VETO_LOCATION_REGIONS;
    });
    __webpack_require__.d(__webpack_exports__, "c", function() {
      return MATCH_ROOM_VETO_MAP_ITEMS;
    });
    __webpack_require__.d(__webpack_exports__, "a", function() {
      return DEFAULTS;
    });
    const UPDATE_NOTIFICATION_TYPES = [ "tab", "badge", "disabled" ];
    const MATCH_ROOM_VETO_LOCATION_ITEMS = {
      EU: [ "UK", "Sweden", "France", "Germany", "Netherlands" ],
      US: [ "Chicago", "Dallas", "Denver" ],
      Oceania: [ "Sydney", "Melbourne" ]
    };
    const MATCH_ROOM_VETO_LOCATION_REGIONS = Object.keys(MATCH_ROOM_VETO_LOCATION_ITEMS);
    const MATCH_ROOM_VETO_MAP_ITEMS = [ "de_dust2", "de_mirage", "de_overpass", "de_inferno", "de_nuke", "de_cache", "de_train", "de_vertigo" ];
    const DEFAULTS = {
      extensionEnabled: true,
      headerShowElo: true,
      hideFaceitClientHasLandedBanner: true,
      playerProfileLevelProgress: true,
      partyAutoAcceptInvite: false,
      matchQueueAutoReady: false,
      matchRoomShowPlayerStats: false,
      matchRoomAutoCopyServerData: false,
      matchRoomAutoConnectToServer: false,
      matchRoomHidePlayerControls: true,
      matchRoomAutoVetoLocations: false,
      matchRoomAutoVetoLocationItems: MATCH_ROOM_VETO_LOCATION_ITEMS,
      matchRoomAutoVetoMaps: false,
      matchRoomAutoVetoMapsShuffle: false,
      matchRoomAutoVetoMapsShuffleAmount: 3,
      matchRoomAutoVetoMapItems: MATCH_ROOM_VETO_MAP_ITEMS,
      matchRoomFocusMode: false,
      modalCloseMatchVictory: false,
      modalCloseMatchDefeat: false,
      modalCloseGlobalRankingUpdate: false,
      modalClickInactiveCheck: false,
      notifyDisabled: false,
      notifyPartyAutoAcceptInvite: true,
      notifyMatchQueueAutoReady: true,
      notifyMatchRoomAutoCopyServerData: true,
      notifyMatchRoomAutoConnectToServer: true,
      notifyMatchRoomAutoVetoLocations: true,
      notifyMatchRoomAutoVetoMaps: true,
      updateNotificationType: "tab",
      updateNotifications: [],
      bans: [],
      vips: []
    };
  },
  30: function(module, exports, __webpack_require__) {
    class OptionsSync {
      constructor(storageName = "options") {
        this.storageName = storageName;
      }
      define(defs) {
        defs = Object.assign({
          defaults: {},
          migrations: []
        }, defs);
        if (chrome.runtime.onInstalled) {
          chrome.runtime.onInstalled.addListener(() => this._applyDefinition(defs));
        } else {
          this._applyDefinition(defs);
        }
      }
      async _applyDefinition(defs) {
        const options = Object.assign({}, defs.defaults, await this.getAll());
        console.group("Appling definitions");
        console.info("Current options:", options);
        if (defs.migrations.length > 0) {
          console.info("Running", defs.migrations.length, "migrations");
          defs.migrations.forEach(migrate => migrate(options, defs.defaults));
        }
        console.groupEnd();
        this.setAll(options);
      }
      _parseNumbers(options) {
        for (const name of Object.keys(options)) {
          if (options[name] === String(Number(options[name]))) {
            options[name] = Number(options[name]);
          }
        }
        return options;
      }
      getAll() {
        return new Promise(resolve => {
          chrome.storage.sync.get(this.storageName, keys => resolve(keys[this.storageName] || {}));
        }).then(this._parseNumbers);
      }
      setAll(newOptions) {
        return new Promise(resolve => {
          chrome.storage.sync.set({
            [this.storageName]: newOptions
          }, resolve);
        });
      }
      async set(newOptions) {
        const options = await this.getAll();
        this.setAll(Object.assign(options, newOptions));
      }
      syncForm(form) {
        if (typeof form === "string") {
          form = document.querySelector(form);
        }
        this.getAll().then(options => OptionsSync._applyToForm(options, form));
        form.addEventListener("input", e => this._handleFormUpdatesDebounced(e));
        form.addEventListener("change", e => this._handleFormUpdatesDebounced(e));
        chrome.storage.onChanged.addListener((changes, namespace) => {
          if (namespace === "sync") {
            for (const key of Object.keys(changes)) {
              const {newValue: newValue} = changes[key];
              if (key === this.storageName) {
                OptionsSync._applyToForm(newValue, form);
                return;
              }
            }
          }
        });
      }
      static _applyToForm(options, form) {
        console.group("Updating form");
        for (const name of Object.keys(options)) {
          const els = form.querySelectorAll(`[name="${name}"]`);
          const [field] = els;
          if (field) {
            console.info(name, ":", options[name]);
            switch (field.type) {
             case "checkbox":
              field.checked = options[name];
              break;

             case "radio":
              {
                const [selected] = [ ...els ].filter(el => el.value === options[name]);
                if (selected) {
                  selected.checked = true;
                }
                break;
              }

             default:
              field.value = options[name];
              break;
            }
            field.dispatchEvent(new InputEvent("input"));
          } else {
            console.warn("Stored option {", name, ":", options[name], "} was not found on the page");
          }
        }
        console.groupEnd();
      }
      _handleFormUpdatesDebounced({target: el}) {
        if (this.timer) {
          clearTimeout(this.timer);
        }
        this.timer = setTimeout(() => {
          this._handleFormUpdates(el);
          this.timer = undefined;
        }, 100);
      }
      _handleFormUpdates(el) {
        const {name: name} = el;
        let {value: value} = el;
        if (!name || !el.validity.valid) {
          return;
        }
        switch (el.type) {
         case "select-one":
          value = el.options[el.selectedIndex].value;
          break;

         case "checkbox":
          value = el.checked;
          break;

         default:
          break;
        }
        console.info("Saving option", el.name, "to", value);
        this.set({
          [name]: value
        });
      }
    }
    OptionsSync.migrations = {
      removeUnused(options, defaults) {
        for (const key of Object.keys(options)) {
          if (!(key in defaults)) {
            delete options[key];
          }
        }
      }
    };
    if (typeof HTMLElement !== "undefined") {
      class OptionsSyncElement extends HTMLElement {
        constructor() {
          super();
          new OptionsSync(this.getAttribute("storageName") || undefined).syncForm(this);
        }
      }
      try {
        customElements.define("options-sync", OptionsSyncElement);
      } catch (error) {}
    }
    if (true) {
      module.exports = OptionsSync;
    }
  },
  334: function(module, exports, __webpack_require__) {
    "use strict";
    var semver = __webpack_require__(558);
    module.exports = function(a, b) {
      if (semver.gt(a, b)) {
        return null;
      }
      a = semver.parse(a);
      b = semver.parse(b);
      for (var key in a) {
        if (key === "major" || key === "minor" || key === "patch") {
          if (a[key] !== b[key]) {
            return key;
          }
        }
        if (key === "prerelease" || key === "build") {
          if (JSON.stringify(a[key]) !== JSON.stringify(b[key])) {
            return key;
          }
        }
      }
      return null;
    };
  },
  335: function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    (function(global) {
      const getGlobal = property => {
        if (typeof self !== "undefined" && self && property in self) {
          return self[property];
        }
        if (typeof window !== "undefined" && window && property in window) {
          return window[property];
        }
        if (typeof global !== "undefined" && global && property in global) {
          return global[property];
        }
        if (typeof globalThis !== "undefined" && globalThis) {
          return globalThis[property];
        }
      };
      const document = getGlobal("document");
      const Headers = getGlobal("Headers");
      const Response = getGlobal("Response");
      const ReadableStream = getGlobal("ReadableStream");
      const fetch = getGlobal("fetch");
      const AbortController = getGlobal("AbortController");
      const FormData = getGlobal("FormData");
      const isObject = value => value !== null && typeof value === "object";
      const supportsAbortController = typeof AbortController === "function";
      const supportsStreams = typeof ReadableStream === "function";
      const supportsFormData = typeof FormData === "function";
      const deepMerge = (...sources) => {
        let returnValue = {};
        for (const source of sources) {
          if (Array.isArray(source)) {
            if (!Array.isArray(returnValue)) {
              returnValue = [];
            }
            returnValue = [ ...returnValue, ...source ];
          } else if (isObject(source)) {
            for (let [key, value] of Object.entries(source)) {
              if (isObject(value) && Reflect.has(returnValue, key)) {
                value = deepMerge(returnValue[key], value);
              }
              returnValue = {
                ...returnValue,
                [key]: value
              };
            }
          }
        }
        return returnValue;
      };
      const requestMethods = [ "get", "post", "put", "patch", "head", "delete" ];
      const responseTypes = {
        json: "application/json",
        text: "text/*",
        formData: "multipart/form-data",
        arrayBuffer: "*/*",
        blob: "*/*"
      };
      const retryMethods = new Set([ "get", "put", "head", "delete", "options", "trace" ]);
      const retryStatusCodes = new Set([ 408, 413, 429, 500, 502, 503, 504 ]);
      const retryAfterStatusCodes = new Set([ 413, 429, 503 ]);
      class HTTPError extends Error {
        constructor(response) {
          super(response.statusText);
          this.name = "HTTPError";
          this.response = response;
        }
      }
      class TimeoutError extends Error {
        constructor() {
          super("Request timed out");
          this.name = "TimeoutError";
        }
      }
      const safeTimeout = (resolve, reject, ms) => {
        if (ms > 2147483647) {
          reject(new RangeError("The `timeout` option cannot be greater than 2147483647"));
        }
        return setTimeout(resolve, ms);
      };
      const delay = ms => new Promise((resolve, reject) => safeTimeout(resolve, reject, ms));
      const timeout = (promise, ms, abortController) => new Promise((resolve, reject) => {
        const timeoutID = safeTimeout(() => {
          if (supportsAbortController) {
            abortController.abort();
          }
          reject(new TimeoutError());
        }, reject, ms);
        promise.then(resolve).catch(reject).then(() => {
          clearTimeout(timeoutID);
        });
      });
      const normalizeRequestMethod = input => requestMethods.includes(input) ? input.toUpperCase() : input;
      class Ky {
        constructor(input, {timeout: timeout = 1e4, hooks: hooks, throwHttpErrors: throwHttpErrors = true, searchParams: searchParams, json: json, ...otherOptions}) {
          this._retryCount = 0;
          this._options = {
            method: "get",
            credentials: "same-origin",
            retry: 2,
            ...otherOptions
          };
          if (supportsAbortController) {
            this.abortController = new AbortController();
            if (this._options.signal) {
              this._options.signal.addEventListener("abort", () => {
                this.abortController.abort();
              });
            }
            this._options.signal = this.abortController.signal;
          }
          this._options.method = normalizeRequestMethod(this._options.method);
          this._options.prefixUrl = String(this._options.prefixUrl || "");
          this._input = String(input || "");
          if (this._options.prefixUrl && this._input.startsWith("/")) {
            throw new Error("`input` must not begin with a slash when using `prefixUrl`");
          }
          if (this._options.prefixUrl && !this._options.prefixUrl.endsWith("/")) {
            this._options.prefixUrl += "/";
          }
          this._input = this._options.prefixUrl + this._input;
          if (searchParams) {
            const url = new URL(this._input, document && document.baseURI);
            if (typeof searchParams === "string" || URLSearchParams && searchParams instanceof URLSearchParams) {
              url.search = searchParams;
            } else if (Object.values(searchParams).every(param => typeof param === "number" || typeof param === "string")) {
              url.search = new URLSearchParams(searchParams).toString();
            } else {
              throw new Error("The `searchParams` option must be either a string, `URLSearchParams` instance or an object with string and number values");
            }
            this._input = url.toString();
          }
          this._timeout = timeout;
          this._hooks = deepMerge({
            beforeRequest: [],
            afterResponse: []
          }, hooks);
          this._throwHttpErrors = throwHttpErrors;
          const headers = new Headers(this._options.headers || {});
          if ((supportsFormData && this._options.body instanceof FormData || this._options.body instanceof URLSearchParams) && headers.has("content-type")) {
            throw new Error(`The \`content-type\` header cannot be used with a ${this._options.body.constructor.name} body. It will be set automatically.`);
          }
          if (json) {
            if (this._options.body) {
              throw new Error("The `json` option cannot be used with the `body` option");
            }
            headers.set("content-type", "application/json");
            this._options.body = JSON.stringify(json);
          }
          this._options.headers = headers;
          const fn = async () => {
            await delay(1);
            let response = await this._fetch();
            for (const hook of this._hooks.afterResponse) {
              const modifiedResponse = await hook(response.clone());
              if (modifiedResponse instanceof Response) {
                response = modifiedResponse;
              }
            }
            if (!response.ok && this._throwHttpErrors) {
              throw new HTTPError(response);
            }
            if (this._options.onDownloadProgress) {
              if (typeof this._options.onDownloadProgress !== "function") {
                throw new TypeError("The `onDownloadProgress` option must be a function");
              }
              if (!supportsStreams) {
                throw new Error("Streams are not supported in your environment. `ReadableStream` is missing.");
              }
              return this._stream(response.clone(), this._options.onDownloadProgress);
            }
            return response;
          };
          const isRetriableMethod = retryMethods.has(this._options.method.toLowerCase());
          const result = isRetriableMethod ? this._retry(fn) : fn();
          for (const [type, mimeType] of Object.entries(responseTypes)) {
            result[type] = (async () => {
              headers.set("accept", mimeType);
              return (await result).clone()[type]();
            });
          }
          return result;
        }
        _calculateRetryDelay(error) {
          this._retryCount++;
          if (this._retryCount < this._options.retry && !(error instanceof TimeoutError)) {
            if (error instanceof HTTPError) {
              if (!retryStatusCodes.has(error.response.status)) {
                return 0;
              }
              const retryAfter = error.response.headers.get("Retry-After");
              if (retryAfter && retryAfterStatusCodes.has(error.response.status)) {
                let after = Number(retryAfter);
                if (Number.isNaN(after)) {
                  after = Date.parse(retryAfter) - Date.now();
                } else {
                  after *= 1e3;
                }
                return after;
              }
              if (error.response.status === 413) {
                return 0;
              }
            }
            const BACKOFF_FACTOR = .3;
            return BACKOFF_FACTOR * 2 ** (this._retryCount - 1) * 1e3;
          }
          return 0;
        }
        async _retry(fn) {
          try {
            return await fn();
          } catch (error) {
            const ms = this._calculateRetryDelay(error);
            if (ms !== 0 && this._retryCount > 0) {
              await delay(ms);
              return this._retry(fn);
            }
            if (this._throwHttpErrors) {
              throw error;
            }
          }
        }
        async _fetch() {
          for (const hook of this._hooks.beforeRequest) {
            await hook(this._options);
          }
          if (this._timeout === false) {
            return fetch(this._input, this._options);
          }
          return timeout(fetch(this._input, this._options), this._timeout, this.abortController);
        }
        _stream(response, onDownloadProgress) {
          const totalBytes = Number(response.headers.get("content-length")) || 0;
          let transferredBytes = 0;
          return new Response(new ReadableStream({
            start(controller) {
              const reader = response.body.getReader();
              if (onDownloadProgress) {
                onDownloadProgress({
                  percent: 0,
                  transferredBytes: 0,
                  totalBytes: totalBytes
                }, new Uint8Array());
              }
              async function read() {
                const {done: done, value: value} = await reader.read();
                if (done) {
                  controller.close();
                  return;
                }
                if (onDownloadProgress) {
                  transferredBytes += value.byteLength;
                  const percent = totalBytes === 0 ? 0 : transferredBytes / totalBytes;
                  onDownloadProgress({
                    percent: percent,
                    transferredBytes: transferredBytes,
                    totalBytes: totalBytes
                  }, value);
                }
                controller.enqueue(value);
                read();
              }
              read();
            }
          }));
        }
      }
      const validateAndMerge = (...sources) => {
        for (const source of sources) {
          if ((!isObject(source) || Array.isArray(source)) && typeof source !== "undefined") {
            throw new TypeError("The `options` argument must be an object");
          }
        }
        return deepMerge({}, ...sources);
      };
      const createInstance = defaults => {
        const ky = (input, options) => new Ky(input, validateAndMerge(defaults, options));
        for (const method of requestMethods) {
          ky[method] = ((input, options) => new Ky(input, validateAndMerge(defaults, options, {
            method: method
          })));
        }
        ky.create = (newDefaults => createInstance(validateAndMerge(newDefaults)));
        ky.extend = (newDefaults => createInstance(validateAndMerge(defaults, newDefaults)));
        return ky;
      };
      __webpack_exports__["a"] = createInstance();
    }).call(this, __webpack_require__(36));
  },
  36: function(module, exports) {
    var g;
    g = function() {
      return this;
    }();
    try {
      g = g || Function("return this")() || (1, eval)("this");
    } catch (e) {
      if (typeof window === "object") g = window;
    }
    module.exports = g;
  },
  40: function(module) {
    module.exports = {
      "2.9.0": "https://redd.it/dds14w",
      "2.0.0": "https://redd.it/bkx7tl",
      "1.6.7": "https://redd.it/9afkzy",
      "1.6.6": "https://redd.it/9afkzy",
      "1.4.1": "https://redd.it/8kd917",
      "1.4.0": "https://redd.it/8kd917",
      "1.3.3": "https://redd.it/8f2jxc",
      "1.3.2": "https://redd.it/8f2jxc",
      "1.3.1": "https://redd.it/8f2jxc",
      "1.3.0": "https://redd.it/8f2jxc",
      "1.2.2": "https://redd.it/8cppf1",
      "1.2.1": "https://redd.it/8cppf1",
      "1.2.0": "https://redd.it/8cppf1",
      "1.1.0": "https://redd.it/8b6mqw",
      "1.0.2": "https://redd.it/89rwwm",
      "1.0.1": "https://redd.it/89rwwm",
      "1.0.0": "https://redd.it/89rwwm",
      "0.18.1": "https://redd.it/87iuka",
      "0.18.0": "https://redd.it/87iuka",
      "0.17.1": "https://redd.it/87d0g3",
      "0.17.0": "https://redd.it/87d0g3",
      "0.16.2": "https://redd.it/871ffx",
      "0.16.1": "https://redd.it/871ffx",
      "0.16.0": "https://redd.it/871ffx",
      "0.15.5": "https://redd.it/86mi95",
      "0.15.4": "https://redd.it/86mi95",
      "0.15.3": "https://redd.it/86mi95",
      "0.15.2": "https://redd.it/86mi95",
      "0.15.1": "https://redd.it/86mi95",
      "0.15.0": "https://redd.it/86mi95",
      "0.14.3": "https://redd.it/86d9fw",
      "0.14.2": "https://redd.it/86d9fw",
      "0.14.1": "https://redd.it/86d9fw",
      "0.14.0": "https://redd.it/86d9fw",
      "0.13.0": "https://redd.it/85ud1b",
      "0.12.1": "https://redd.it/85o957",
      "0.12.0": "https://redd.it/85o957",
      "0.11.3": "https://redd.it/85et26",
      "0.11.2": "https://redd.it/85et26",
      "0.11.1": "https://redd.it/85et26",
      "0.11.0": "https://redd.it/85et26",
      "0.10.3": "https://redd.it/850ife",
      "0.10.2": "https://redd.it/850ife",
      "0.10.1": "https://redd.it/850ife",
      "0.10.0": "https://redd.it/850ife",
      "0.9.0": "https://redd.it/84i064",
      "0.8.0": "https://redd.it/84f9n9",
      "0.7.0": "https://redd.it/847yin"
    };
  },
  558: function(module, exports, __webpack_require__) {
    (function(process) {
      exports = module.exports = SemVer;
      var debug;
      if (typeof process === "object" && process.env && process.env.NODE_DEBUG && /\bsemver\b/i.test(process.env.NODE_DEBUG)) {
        debug = function() {
          var args = Array.prototype.slice.call(arguments, 0);
          args.unshift("SEMVER");
          console.log.apply(console, args);
        };
      } else {
        debug = function() {};
      }
      exports.SEMVER_SPEC_VERSION = "2.0.0";
      var MAX_LENGTH = 256;
      var MAX_SAFE_INTEGER = Number.MAX_SAFE_INTEGER || 9007199254740991;
      var MAX_SAFE_COMPONENT_LENGTH = 16;
      var re = exports.re = [];
      var src = exports.src = [];
      var R = 0;
      var NUMERICIDENTIFIER = R++;
      src[NUMERICIDENTIFIER] = "0|[1-9]\\d*";
      var NUMERICIDENTIFIERLOOSE = R++;
      src[NUMERICIDENTIFIERLOOSE] = "[0-9]+";
      var NONNUMERICIDENTIFIER = R++;
      src[NONNUMERICIDENTIFIER] = "\\d*[a-zA-Z-][a-zA-Z0-9-]*";
      var MAINVERSION = R++;
      src[MAINVERSION] = "(" + src[NUMERICIDENTIFIER] + ")\\." + "(" + src[NUMERICIDENTIFIER] + ")\\." + "(" + src[NUMERICIDENTIFIER] + ")";
      var MAINVERSIONLOOSE = R++;
      src[MAINVERSIONLOOSE] = "(" + src[NUMERICIDENTIFIERLOOSE] + ")\\." + "(" + src[NUMERICIDENTIFIERLOOSE] + ")\\." + "(" + src[NUMERICIDENTIFIERLOOSE] + ")";
      var PRERELEASEIDENTIFIER = R++;
      src[PRERELEASEIDENTIFIER] = "(?:" + src[NUMERICIDENTIFIER] + "|" + src[NONNUMERICIDENTIFIER] + ")";
      var PRERELEASEIDENTIFIERLOOSE = R++;
      src[PRERELEASEIDENTIFIERLOOSE] = "(?:" + src[NUMERICIDENTIFIERLOOSE] + "|" + src[NONNUMERICIDENTIFIER] + ")";
      var PRERELEASE = R++;
      src[PRERELEASE] = "(?:-(" + src[PRERELEASEIDENTIFIER] + "(?:\\." + src[PRERELEASEIDENTIFIER] + ")*))";
      var PRERELEASELOOSE = R++;
      src[PRERELEASELOOSE] = "(?:-?(" + src[PRERELEASEIDENTIFIERLOOSE] + "(?:\\." + src[PRERELEASEIDENTIFIERLOOSE] + ")*))";
      var BUILDIDENTIFIER = R++;
      src[BUILDIDENTIFIER] = "[0-9A-Za-z-]+";
      var BUILD = R++;
      src[BUILD] = "(?:\\+(" + src[BUILDIDENTIFIER] + "(?:\\." + src[BUILDIDENTIFIER] + ")*))";
      var FULL = R++;
      var FULLPLAIN = "v?" + src[MAINVERSION] + src[PRERELEASE] + "?" + src[BUILD] + "?";
      src[FULL] = "^" + FULLPLAIN + "$";
      var LOOSEPLAIN = "[v=\\s]*" + src[MAINVERSIONLOOSE] + src[PRERELEASELOOSE] + "?" + src[BUILD] + "?";
      var LOOSE = R++;
      src[LOOSE] = "^" + LOOSEPLAIN + "$";
      var GTLT = R++;
      src[GTLT] = "((?:<|>)?=?)";
      var XRANGEIDENTIFIERLOOSE = R++;
      src[XRANGEIDENTIFIERLOOSE] = src[NUMERICIDENTIFIERLOOSE] + "|x|X|\\*";
      var XRANGEIDENTIFIER = R++;
      src[XRANGEIDENTIFIER] = src[NUMERICIDENTIFIER] + "|x|X|\\*";
      var XRANGEPLAIN = R++;
      src[XRANGEPLAIN] = "[v=\\s]*(" + src[XRANGEIDENTIFIER] + ")" + "(?:\\.(" + src[XRANGEIDENTIFIER] + ")" + "(?:\\.(" + src[XRANGEIDENTIFIER] + ")" + "(?:" + src[PRERELEASE] + ")?" + src[BUILD] + "?" + ")?)?";
      var XRANGEPLAINLOOSE = R++;
      src[XRANGEPLAINLOOSE] = "[v=\\s]*(" + src[XRANGEIDENTIFIERLOOSE] + ")" + "(?:\\.(" + src[XRANGEIDENTIFIERLOOSE] + ")" + "(?:\\.(" + src[XRANGEIDENTIFIERLOOSE] + ")" + "(?:" + src[PRERELEASELOOSE] + ")?" + src[BUILD] + "?" + ")?)?";
      var XRANGE = R++;
      src[XRANGE] = "^" + src[GTLT] + "\\s*" + src[XRANGEPLAIN] + "$";
      var XRANGELOOSE = R++;
      src[XRANGELOOSE] = "^" + src[GTLT] + "\\s*" + src[XRANGEPLAINLOOSE] + "$";
      var COERCE = R++;
      src[COERCE] = "(?:^|[^\\d])" + "(\\d{1," + MAX_SAFE_COMPONENT_LENGTH + "})" + "(?:\\.(\\d{1," + MAX_SAFE_COMPONENT_LENGTH + "}))?" + "(?:\\.(\\d{1," + MAX_SAFE_COMPONENT_LENGTH + "}))?" + "(?:$|[^\\d])";
      var LONETILDE = R++;
      src[LONETILDE] = "(?:~>?)";
      var TILDETRIM = R++;
      src[TILDETRIM] = "(\\s*)" + src[LONETILDE] + "\\s+";
      re[TILDETRIM] = new RegExp(src[TILDETRIM], "g");
      var tildeTrimReplace = "$1~";
      var TILDE = R++;
      src[TILDE] = "^" + src[LONETILDE] + src[XRANGEPLAIN] + "$";
      var TILDELOOSE = R++;
      src[TILDELOOSE] = "^" + src[LONETILDE] + src[XRANGEPLAINLOOSE] + "$";
      var LONECARET = R++;
      src[LONECARET] = "(?:\\^)";
      var CARETTRIM = R++;
      src[CARETTRIM] = "(\\s*)" + src[LONECARET] + "\\s+";
      re[CARETTRIM] = new RegExp(src[CARETTRIM], "g");
      var caretTrimReplace = "$1^";
      var CARET = R++;
      src[CARET] = "^" + src[LONECARET] + src[XRANGEPLAIN] + "$";
      var CARETLOOSE = R++;
      src[CARETLOOSE] = "^" + src[LONECARET] + src[XRANGEPLAINLOOSE] + "$";
      var COMPARATORLOOSE = R++;
      src[COMPARATORLOOSE] = "^" + src[GTLT] + "\\s*(" + LOOSEPLAIN + ")$|^$";
      var COMPARATOR = R++;
      src[COMPARATOR] = "^" + src[GTLT] + "\\s*(" + FULLPLAIN + ")$|^$";
      var COMPARATORTRIM = R++;
      src[COMPARATORTRIM] = "(\\s*)" + src[GTLT] + "\\s*(" + LOOSEPLAIN + "|" + src[XRANGEPLAIN] + ")";
      re[COMPARATORTRIM] = new RegExp(src[COMPARATORTRIM], "g");
      var comparatorTrimReplace = "$1$2$3";
      var HYPHENRANGE = R++;
      src[HYPHENRANGE] = "^\\s*(" + src[XRANGEPLAIN] + ")" + "\\s+-\\s+" + "(" + src[XRANGEPLAIN] + ")" + "\\s*$";
      var HYPHENRANGELOOSE = R++;
      src[HYPHENRANGELOOSE] = "^\\s*(" + src[XRANGEPLAINLOOSE] + ")" + "\\s+-\\s+" + "(" + src[XRANGEPLAINLOOSE] + ")" + "\\s*$";
      var STAR = R++;
      src[STAR] = "(<|>)?=?\\s*\\*";
      for (var i = 0; i < R; i++) {
        debug(i, src[i]);
        if (!re[i]) {
          re[i] = new RegExp(src[i]);
        }
      }
      exports.parse = parse;
      function parse(version, options) {
        if (!options || typeof options !== "object") {
          options = {
            loose: !!options,
            includePrerelease: false
          };
        }
        if (version instanceof SemVer) {
          return version;
        }
        if (typeof version !== "string") {
          return null;
        }
        if (version.length > MAX_LENGTH) {
          return null;
        }
        var r = options.loose ? re[LOOSE] : re[FULL];
        if (!r.test(version)) {
          return null;
        }
        try {
          return new SemVer(version, options);
        } catch (er) {
          return null;
        }
      }
      exports.valid = valid;
      function valid(version, options) {
        var v = parse(version, options);
        return v ? v.version : null;
      }
      exports.clean = clean;
      function clean(version, options) {
        var s = parse(version.trim().replace(/^[=v]+/, ""), options);
        return s ? s.version : null;
      }
      exports.SemVer = SemVer;
      function SemVer(version, options) {
        if (!options || typeof options !== "object") {
          options = {
            loose: !!options,
            includePrerelease: false
          };
        }
        if (version instanceof SemVer) {
          if (version.loose === options.loose) {
            return version;
          } else {
            version = version.version;
          }
        } else if (typeof version !== "string") {
          throw new TypeError("Invalid Version: " + version);
        }
        if (version.length > MAX_LENGTH) {
          throw new TypeError("version is longer than " + MAX_LENGTH + " characters");
        }
        if (!(this instanceof SemVer)) {
          return new SemVer(version, options);
        }
        debug("SemVer", version, options);
        this.options = options;
        this.loose = !!options.loose;
        var m = version.trim().match(options.loose ? re[LOOSE] : re[FULL]);
        if (!m) {
          throw new TypeError("Invalid Version: " + version);
        }
        this.raw = version;
        this.major = +m[1];
        this.minor = +m[2];
        this.patch = +m[3];
        if (this.major > MAX_SAFE_INTEGER || this.major < 0) {
          throw new TypeError("Invalid major version");
        }
        if (this.minor > MAX_SAFE_INTEGER || this.minor < 0) {
          throw new TypeError("Invalid minor version");
        }
        if (this.patch > MAX_SAFE_INTEGER || this.patch < 0) {
          throw new TypeError("Invalid patch version");
        }
        if (!m[4]) {
          this.prerelease = [];
        } else {
          this.prerelease = m[4].split(".").map(function(id) {
            if (/^[0-9]+$/.test(id)) {
              var num = +id;
              if (num >= 0 && num < MAX_SAFE_INTEGER) {
                return num;
              }
            }
            return id;
          });
        }
        this.build = m[5] ? m[5].split(".") : [];
        this.format();
      }
      SemVer.prototype.format = function() {
        this.version = this.major + "." + this.minor + "." + this.patch;
        if (this.prerelease.length) {
          this.version += "-" + this.prerelease.join(".");
        }
        return this.version;
      };
      SemVer.prototype.toString = function() {
        return this.version;
      };
      SemVer.prototype.compare = function(other) {
        debug("SemVer.compare", this.version, this.options, other);
        if (!(other instanceof SemVer)) {
          other = new SemVer(other, this.options);
        }
        return this.compareMain(other) || this.comparePre(other);
      };
      SemVer.prototype.compareMain = function(other) {
        if (!(other instanceof SemVer)) {
          other = new SemVer(other, this.options);
        }
        return compareIdentifiers(this.major, other.major) || compareIdentifiers(this.minor, other.minor) || compareIdentifiers(this.patch, other.patch);
      };
      SemVer.prototype.comparePre = function(other) {
        if (!(other instanceof SemVer)) {
          other = new SemVer(other, this.options);
        }
        if (this.prerelease.length && !other.prerelease.length) {
          return -1;
        } else if (!this.prerelease.length && other.prerelease.length) {
          return 1;
        } else if (!this.prerelease.length && !other.prerelease.length) {
          return 0;
        }
        var i = 0;
        do {
          var a = this.prerelease[i];
          var b = other.prerelease[i];
          debug("prerelease compare", i, a, b);
          if (a === undefined && b === undefined) {
            return 0;
          } else if (b === undefined) {
            return 1;
          } else if (a === undefined) {
            return -1;
          } else if (a === b) {
            continue;
          } else {
            return compareIdentifiers(a, b);
          }
        } while (++i);
      };
      SemVer.prototype.inc = function(release, identifier) {
        switch (release) {
         case "premajor":
          this.prerelease.length = 0;
          this.patch = 0;
          this.minor = 0;
          this.major++;
          this.inc("pre", identifier);
          break;

         case "preminor":
          this.prerelease.length = 0;
          this.patch = 0;
          this.minor++;
          this.inc("pre", identifier);
          break;

         case "prepatch":
          this.prerelease.length = 0;
          this.inc("patch", identifier);
          this.inc("pre", identifier);
          break;

         case "prerelease":
          if (this.prerelease.length === 0) {
            this.inc("patch", identifier);
          }
          this.inc("pre", identifier);
          break;

         case "major":
          if (this.minor !== 0 || this.patch !== 0 || this.prerelease.length === 0) {
            this.major++;
          }
          this.minor = 0;
          this.patch = 0;
          this.prerelease = [];
          break;

         case "minor":
          if (this.patch !== 0 || this.prerelease.length === 0) {
            this.minor++;
          }
          this.patch = 0;
          this.prerelease = [];
          break;

         case "patch":
          if (this.prerelease.length === 0) {
            this.patch++;
          }
          this.prerelease = [];
          break;

         case "pre":
          if (this.prerelease.length === 0) {
            this.prerelease = [ 0 ];
          } else {
            var i = this.prerelease.length;
            while (--i >= 0) {
              if (typeof this.prerelease[i] === "number") {
                this.prerelease[i]++;
                i = -2;
              }
            }
            if (i === -1) {
              this.prerelease.push(0);
            }
          }
          if (identifier) {
            if (this.prerelease[0] === identifier) {
              if (isNaN(this.prerelease[1])) {
                this.prerelease = [ identifier, 0 ];
              }
            } else {
              this.prerelease = [ identifier, 0 ];
            }
          }
          break;

         default:
          throw new Error("invalid increment argument: " + release);
        }
        this.format();
        this.raw = this.version;
        return this;
      };
      exports.inc = inc;
      function inc(version, release, loose, identifier) {
        if (typeof loose === "string") {
          identifier = loose;
          loose = undefined;
        }
        try {
          return new SemVer(version, loose).inc(release, identifier).version;
        } catch (er) {
          return null;
        }
      }
      exports.diff = diff;
      function diff(version1, version2) {
        if (eq(version1, version2)) {
          return null;
        } else {
          var v1 = parse(version1);
          var v2 = parse(version2);
          var prefix = "";
          if (v1.prerelease.length || v2.prerelease.length) {
            prefix = "pre";
            var defaultResult = "prerelease";
          }
          for (var key in v1) {
            if (key === "major" || key === "minor" || key === "patch") {
              if (v1[key] !== v2[key]) {
                return prefix + key;
              }
            }
          }
          return defaultResult;
        }
      }
      exports.compareIdentifiers = compareIdentifiers;
      var numeric = /^[0-9]+$/;
      function compareIdentifiers(a, b) {
        var anum = numeric.test(a);
        var bnum = numeric.test(b);
        if (anum && bnum) {
          a = +a;
          b = +b;
        }
        return a === b ? 0 : anum && !bnum ? -1 : bnum && !anum ? 1 : a < b ? -1 : 1;
      }
      exports.rcompareIdentifiers = rcompareIdentifiers;
      function rcompareIdentifiers(a, b) {
        return compareIdentifiers(b, a);
      }
      exports.major = major;
      function major(a, loose) {
        return new SemVer(a, loose).major;
      }
      exports.minor = minor;
      function minor(a, loose) {
        return new SemVer(a, loose).minor;
      }
      exports.patch = patch;
      function patch(a, loose) {
        return new SemVer(a, loose).patch;
      }
      exports.compare = compare;
      function compare(a, b, loose) {
        return new SemVer(a, loose).compare(new SemVer(b, loose));
      }
      exports.compareLoose = compareLoose;
      function compareLoose(a, b) {
        return compare(a, b, true);
      }
      exports.rcompare = rcompare;
      function rcompare(a, b, loose) {
        return compare(b, a, loose);
      }
      exports.sort = sort;
      function sort(list, loose) {
        return list.sort(function(a, b) {
          return exports.compare(a, b, loose);
        });
      }
      exports.rsort = rsort;
      function rsort(list, loose) {
        return list.sort(function(a, b) {
          return exports.rcompare(a, b, loose);
        });
      }
      exports.gt = gt;
      function gt(a, b, loose) {
        return compare(a, b, loose) > 0;
      }
      exports.lt = lt;
      function lt(a, b, loose) {
        return compare(a, b, loose) < 0;
      }
      exports.eq = eq;
      function eq(a, b, loose) {
        return compare(a, b, loose) === 0;
      }
      exports.neq = neq;
      function neq(a, b, loose) {
        return compare(a, b, loose) !== 0;
      }
      exports.gte = gte;
      function gte(a, b, loose) {
        return compare(a, b, loose) >= 0;
      }
      exports.lte = lte;
      function lte(a, b, loose) {
        return compare(a, b, loose) <= 0;
      }
      exports.cmp = cmp;
      function cmp(a, op, b, loose) {
        switch (op) {
         case "===":
          if (typeof a === "object") a = a.version;
          if (typeof b === "object") b = b.version;
          return a === b;

         case "!==":
          if (typeof a === "object") a = a.version;
          if (typeof b === "object") b = b.version;
          return a !== b;

         case "":
         case "=":
         case "==":
          return eq(a, b, loose);

         case "!=":
          return neq(a, b, loose);

         case ">":
          return gt(a, b, loose);

         case ">=":
          return gte(a, b, loose);

         case "<":
          return lt(a, b, loose);

         case "<=":
          return lte(a, b, loose);

         default:
          throw new TypeError("Invalid operator: " + op);
        }
      }
      exports.Comparator = Comparator;
      function Comparator(comp, options) {
        if (!options || typeof options !== "object") {
          options = {
            loose: !!options,
            includePrerelease: false
          };
        }
        if (comp instanceof Comparator) {
          if (comp.loose === !!options.loose) {
            return comp;
          } else {
            comp = comp.value;
          }
        }
        if (!(this instanceof Comparator)) {
          return new Comparator(comp, options);
        }
        debug("comparator", comp, options);
        this.options = options;
        this.loose = !!options.loose;
        this.parse(comp);
        if (this.semver === ANY) {
          this.value = "";
        } else {
          this.value = this.operator + this.semver.version;
        }
        debug("comp", this);
      }
      var ANY = {};
      Comparator.prototype.parse = function(comp) {
        var r = this.options.loose ? re[COMPARATORLOOSE] : re[COMPARATOR];
        var m = comp.match(r);
        if (!m) {
          throw new TypeError("Invalid comparator: " + comp);
        }
        this.operator = m[1];
        if (this.operator === "=") {
          this.operator = "";
        }
        if (!m[2]) {
          this.semver = ANY;
        } else {
          this.semver = new SemVer(m[2], this.options.loose);
        }
      };
      Comparator.prototype.toString = function() {
        return this.value;
      };
      Comparator.prototype.test = function(version) {
        debug("Comparator.test", version, this.options.loose);
        if (this.semver === ANY) {
          return true;
        }
        if (typeof version === "string") {
          version = new SemVer(version, this.options);
        }
        return cmp(version, this.operator, this.semver, this.options);
      };
      Comparator.prototype.intersects = function(comp, options) {
        if (!(comp instanceof Comparator)) {
          throw new TypeError("a Comparator is required");
        }
        if (!options || typeof options !== "object") {
          options = {
            loose: !!options,
            includePrerelease: false
          };
        }
        var rangeTmp;
        if (this.operator === "") {
          rangeTmp = new Range(comp.value, options);
          return satisfies(this.value, rangeTmp, options);
        } else if (comp.operator === "") {
          rangeTmp = new Range(this.value, options);
          return satisfies(comp.semver, rangeTmp, options);
        }
        var sameDirectionIncreasing = (this.operator === ">=" || this.operator === ">") && (comp.operator === ">=" || comp.operator === ">");
        var sameDirectionDecreasing = (this.operator === "<=" || this.operator === "<") && (comp.operator === "<=" || comp.operator === "<");
        var sameSemVer = this.semver.version === comp.semver.version;
        var differentDirectionsInclusive = (this.operator === ">=" || this.operator === "<=") && (comp.operator === ">=" || comp.operator === "<=");
        var oppositeDirectionsLessThan = cmp(this.semver, "<", comp.semver, options) && ((this.operator === ">=" || this.operator === ">") && (comp.operator === "<=" || comp.operator === "<"));
        var oppositeDirectionsGreaterThan = cmp(this.semver, ">", comp.semver, options) && ((this.operator === "<=" || this.operator === "<") && (comp.operator === ">=" || comp.operator === ">"));
        return sameDirectionIncreasing || sameDirectionDecreasing || sameSemVer && differentDirectionsInclusive || oppositeDirectionsLessThan || oppositeDirectionsGreaterThan;
      };
      exports.Range = Range;
      function Range(range, options) {
        if (!options || typeof options !== "object") {
          options = {
            loose: !!options,
            includePrerelease: false
          };
        }
        if (range instanceof Range) {
          if (range.loose === !!options.loose && range.includePrerelease === !!options.includePrerelease) {
            return range;
          } else {
            return new Range(range.raw, options);
          }
        }
        if (range instanceof Comparator) {
          return new Range(range.value, options);
        }
        if (!(this instanceof Range)) {
          return new Range(range, options);
        }
        this.options = options;
        this.loose = !!options.loose;
        this.includePrerelease = !!options.includePrerelease;
        this.raw = range;
        this.set = range.split(/\s*\|\|\s*/).map(function(range) {
          return this.parseRange(range.trim());
        }, this).filter(function(c) {
          return c.length;
        });
        if (!this.set.length) {
          throw new TypeError("Invalid SemVer Range: " + range);
        }
        this.format();
      }
      Range.prototype.format = function() {
        this.range = this.set.map(function(comps) {
          return comps.join(" ").trim();
        }).join("||").trim();
        return this.range;
      };
      Range.prototype.toString = function() {
        return this.range;
      };
      Range.prototype.parseRange = function(range) {
        var loose = this.options.loose;
        range = range.trim();
        var hr = loose ? re[HYPHENRANGELOOSE] : re[HYPHENRANGE];
        range = range.replace(hr, hyphenReplace);
        debug("hyphen replace", range);
        range = range.replace(re[COMPARATORTRIM], comparatorTrimReplace);
        debug("comparator trim", range, re[COMPARATORTRIM]);
        range = range.replace(re[TILDETRIM], tildeTrimReplace);
        range = range.replace(re[CARETTRIM], caretTrimReplace);
        range = range.split(/\s+/).join(" ");
        var compRe = loose ? re[COMPARATORLOOSE] : re[COMPARATOR];
        var set = range.split(" ").map(function(comp) {
          return parseComparator(comp, this.options);
        }, this).join(" ").split(/\s+/);
        if (this.options.loose) {
          set = set.filter(function(comp) {
            return !!comp.match(compRe);
          });
        }
        set = set.map(function(comp) {
          return new Comparator(comp, this.options);
        }, this);
        return set;
      };
      Range.prototype.intersects = function(range, options) {
        if (!(range instanceof Range)) {
          throw new TypeError("a Range is required");
        }
        return this.set.some(function(thisComparators) {
          return thisComparators.every(function(thisComparator) {
            return range.set.some(function(rangeComparators) {
              return rangeComparators.every(function(rangeComparator) {
                return thisComparator.intersects(rangeComparator, options);
              });
            });
          });
        });
      };
      exports.toComparators = toComparators;
      function toComparators(range, options) {
        return new Range(range, options).set.map(function(comp) {
          return comp.map(function(c) {
            return c.value;
          }).join(" ").trim().split(" ");
        });
      }
      function parseComparator(comp, options) {
        debug("comp", comp, options);
        comp = replaceCarets(comp, options);
        debug("caret", comp);
        comp = replaceTildes(comp, options);
        debug("tildes", comp);
        comp = replaceXRanges(comp, options);
        debug("xrange", comp);
        comp = replaceStars(comp, options);
        debug("stars", comp);
        return comp;
      }
      function isX(id) {
        return !id || id.toLowerCase() === "x" || id === "*";
      }
      function replaceTildes(comp, options) {
        return comp.trim().split(/\s+/).map(function(comp) {
          return replaceTilde(comp, options);
        }).join(" ");
      }
      function replaceTilde(comp, options) {
        var r = options.loose ? re[TILDELOOSE] : re[TILDE];
        return comp.replace(r, function(_, M, m, p, pr) {
          debug("tilde", comp, _, M, m, p, pr);
          var ret;
          if (isX(M)) {
            ret = "";
          } else if (isX(m)) {
            ret = ">=" + M + ".0.0 <" + (+M + 1) + ".0.0";
          } else if (isX(p)) {
            ret = ">=" + M + "." + m + ".0 <" + M + "." + (+m + 1) + ".0";
          } else if (pr) {
            debug("replaceTilde pr", pr);
            ret = ">=" + M + "." + m + "." + p + "-" + pr + " <" + M + "." + (+m + 1) + ".0";
          } else {
            ret = ">=" + M + "." + m + "." + p + " <" + M + "." + (+m + 1) + ".0";
          }
          debug("tilde return", ret);
          return ret;
        });
      }
      function replaceCarets(comp, options) {
        return comp.trim().split(/\s+/).map(function(comp) {
          return replaceCaret(comp, options);
        }).join(" ");
      }
      function replaceCaret(comp, options) {
        debug("caret", comp, options);
        var r = options.loose ? re[CARETLOOSE] : re[CARET];
        return comp.replace(r, function(_, M, m, p, pr) {
          debug("caret", comp, _, M, m, p, pr);
          var ret;
          if (isX(M)) {
            ret = "";
          } else if (isX(m)) {
            ret = ">=" + M + ".0.0 <" + (+M + 1) + ".0.0";
          } else if (isX(p)) {
            if (M === "0") {
              ret = ">=" + M + "." + m + ".0 <" + M + "." + (+m + 1) + ".0";
            } else {
              ret = ">=" + M + "." + m + ".0 <" + (+M + 1) + ".0.0";
            }
          } else if (pr) {
            debug("replaceCaret pr", pr);
            if (M === "0") {
              if (m === "0") {
                ret = ">=" + M + "." + m + "." + p + "-" + pr + " <" + M + "." + m + "." + (+p + 1);
              } else {
                ret = ">=" + M + "." + m + "." + p + "-" + pr + " <" + M + "." + (+m + 1) + ".0";
              }
            } else {
              ret = ">=" + M + "." + m + "." + p + "-" + pr + " <" + (+M + 1) + ".0.0";
            }
          } else {
            debug("no pr");
            if (M === "0") {
              if (m === "0") {
                ret = ">=" + M + "." + m + "." + p + " <" + M + "." + m + "." + (+p + 1);
              } else {
                ret = ">=" + M + "." + m + "." + p + " <" + M + "." + (+m + 1) + ".0";
              }
            } else {
              ret = ">=" + M + "." + m + "." + p + " <" + (+M + 1) + ".0.0";
            }
          }
          debug("caret return", ret);
          return ret;
        });
      }
      function replaceXRanges(comp, options) {
        debug("replaceXRanges", comp, options);
        return comp.split(/\s+/).map(function(comp) {
          return replaceXRange(comp, options);
        }).join(" ");
      }
      function replaceXRange(comp, options) {
        comp = comp.trim();
        var r = options.loose ? re[XRANGELOOSE] : re[XRANGE];
        return comp.replace(r, function(ret, gtlt, M, m, p, pr) {
          debug("xRange", comp, ret, gtlt, M, m, p, pr);
          var xM = isX(M);
          var xm = xM || isX(m);
          var xp = xm || isX(p);
          var anyX = xp;
          if (gtlt === "=" && anyX) {
            gtlt = "";
          }
          if (xM) {
            if (gtlt === ">" || gtlt === "<") {
              ret = "<0.0.0";
            } else {
              ret = "*";
            }
          } else if (gtlt && anyX) {
            if (xm) {
              m = 0;
            }
            p = 0;
            if (gtlt === ">") {
              gtlt = ">=";
              if (xm) {
                M = +M + 1;
                m = 0;
                p = 0;
              } else {
                m = +m + 1;
                p = 0;
              }
            } else if (gtlt === "<=") {
              gtlt = "<";
              if (xm) {
                M = +M + 1;
              } else {
                m = +m + 1;
              }
            }
            ret = gtlt + M + "." + m + "." + p;
          } else if (xm) {
            ret = ">=" + M + ".0.0 <" + (+M + 1) + ".0.0";
          } else if (xp) {
            ret = ">=" + M + "." + m + ".0 <" + M + "." + (+m + 1) + ".0";
          }
          debug("xRange return", ret);
          return ret;
        });
      }
      function replaceStars(comp, options) {
        debug("replaceStars", comp, options);
        return comp.trim().replace(re[STAR], "");
      }
      function hyphenReplace($0, from, fM, fm, fp, fpr, fb, to, tM, tm, tp, tpr, tb) {
        if (isX(fM)) {
          from = "";
        } else if (isX(fm)) {
          from = ">=" + fM + ".0.0";
        } else if (isX(fp)) {
          from = ">=" + fM + "." + fm + ".0";
        } else {
          from = ">=" + from;
        }
        if (isX(tM)) {
          to = "";
        } else if (isX(tm)) {
          to = "<" + (+tM + 1) + ".0.0";
        } else if (isX(tp)) {
          to = "<" + tM + "." + (+tm + 1) + ".0";
        } else if (tpr) {
          to = "<=" + tM + "." + tm + "." + tp + "-" + tpr;
        } else {
          to = "<=" + to;
        }
        return (from + " " + to).trim();
      }
      Range.prototype.test = function(version) {
        if (!version) {
          return false;
        }
        if (typeof version === "string") {
          version = new SemVer(version, this.options);
        }
        for (var i = 0; i < this.set.length; i++) {
          if (testSet(this.set[i], version, this.options)) {
            return true;
          }
        }
        return false;
      };
      function testSet(set, version, options) {
        for (var i = 0; i < set.length; i++) {
          if (!set[i].test(version)) {
            return false;
          }
        }
        if (version.prerelease.length && !options.includePrerelease) {
          for (i = 0; i < set.length; i++) {
            debug(set[i].semver);
            if (set[i].semver === ANY) {
              continue;
            }
            if (set[i].semver.prerelease.length > 0) {
              var allowed = set[i].semver;
              if (allowed.major === version.major && allowed.minor === version.minor && allowed.patch === version.patch) {
                return true;
              }
            }
          }
          return false;
        }
        return true;
      }
      exports.satisfies = satisfies;
      function satisfies(version, range, options) {
        try {
          range = new Range(range, options);
        } catch (er) {
          return false;
        }
        return range.test(version);
      }
      exports.maxSatisfying = maxSatisfying;
      function maxSatisfying(versions, range, options) {
        var max = null;
        var maxSV = null;
        try {
          var rangeObj = new Range(range, options);
        } catch (er) {
          return null;
        }
        versions.forEach(function(v) {
          if (rangeObj.test(v)) {
            if (!max || maxSV.compare(v) === -1) {
              max = v;
              maxSV = new SemVer(max, options);
            }
          }
        });
        return max;
      }
      exports.minSatisfying = minSatisfying;
      function minSatisfying(versions, range, options) {
        var min = null;
        var minSV = null;
        try {
          var rangeObj = new Range(range, options);
        } catch (er) {
          return null;
        }
        versions.forEach(function(v) {
          if (rangeObj.test(v)) {
            if (!min || minSV.compare(v) === 1) {
              min = v;
              minSV = new SemVer(min, options);
            }
          }
        });
        return min;
      }
      exports.minVersion = minVersion;
      function minVersion(range, loose) {
        range = new Range(range, loose);
        var minver = new SemVer("0.0.0");
        if (range.test(minver)) {
          return minver;
        }
        minver = new SemVer("0.0.0-0");
        if (range.test(minver)) {
          return minver;
        }
        minver = null;
        for (var i = 0; i < range.set.length; ++i) {
          var comparators = range.set[i];
          comparators.forEach(function(comparator) {
            var compver = new SemVer(comparator.semver.version);
            switch (comparator.operator) {
             case ">":
              if (compver.prerelease.length === 0) {
                compver.patch++;
              } else {
                compver.prerelease.push(0);
              }
              compver.raw = compver.format();

             case "":
             case ">=":
              if (!minver || gt(minver, compver)) {
                minver = compver;
              }
              break;

             case "<":
             case "<=":
              break;

             default:
              throw new Error("Unexpected operation: " + comparator.operator);
            }
          });
        }
        if (minver && range.test(minver)) {
          return minver;
        }
        return null;
      }
      exports.validRange = validRange;
      function validRange(range, options) {
        try {
          return new Range(range, options).range || "*";
        } catch (er) {
          return null;
        }
      }
      exports.ltr = ltr;
      function ltr(version, range, options) {
        return outside(version, range, "<", options);
      }
      exports.gtr = gtr;
      function gtr(version, range, options) {
        return outside(version, range, ">", options);
      }
      exports.outside = outside;
      function outside(version, range, hilo, options) {
        version = new SemVer(version, options);
        range = new Range(range, options);
        var gtfn, ltefn, ltfn, comp, ecomp;
        switch (hilo) {
         case ">":
          gtfn = gt;
          ltefn = lte;
          ltfn = lt;
          comp = ">";
          ecomp = ">=";
          break;

         case "<":
          gtfn = lt;
          ltefn = gte;
          ltfn = gt;
          comp = "<";
          ecomp = "<=";
          break;

         default:
          throw new TypeError('Must provide a hilo val of "<" or ">"');
        }
        if (satisfies(version, range, options)) {
          return false;
        }
        for (var i = 0; i < range.set.length; ++i) {
          var comparators = range.set[i];
          var high = null;
          var low = null;
          comparators.forEach(function(comparator) {
            if (comparator.semver === ANY) {
              comparator = new Comparator(">=0.0.0");
            }
            high = high || comparator;
            low = low || comparator;
            if (gtfn(comparator.semver, high.semver, options)) {
              high = comparator;
            } else if (ltfn(comparator.semver, low.semver, options)) {
              low = comparator;
            }
          });
          if (high.operator === comp || high.operator === ecomp) {
            return false;
          }
          if ((!low.operator || low.operator === comp) && ltefn(version, low.semver)) {
            return false;
          } else if (low.operator === ecomp && ltfn(version, low.semver)) {
            return false;
          }
        }
        return true;
      }
      exports.prerelease = prerelease;
      function prerelease(version, options) {
        var parsed = parse(version, options);
        return parsed && parsed.prerelease.length ? parsed.prerelease : null;
      }
      exports.intersects = intersects;
      function intersects(r1, r2, options) {
        r1 = new Range(r1, options);
        r2 = new Range(r2, options);
        return r1.intersects(r2);
      }
      exports.coerce = coerce;
      function coerce(version) {
        if (version instanceof SemVer) {
          return version;
        }
        if (typeof version !== "string") {
          return null;
        }
        var match = version.match(re[COERCE]);
        if (match == null) {
          return null;
        }
        return parse(match[1] + "." + (match[2] || "0") + "." + (match[3] || "0"));
      }
    }).call(this, __webpack_require__(146));
  },
  559: function(module, exports) {
    (function(i, s, o, g, r, a, m) {
      i["GoogleAnalyticsObject"] = r;
      i[r] = i[r] || function() {
        (i[r].q = i[r].q || []).push(arguments);
      }, i[r].l = 1 * new Date();
      a = s.createElement(o), m = s.getElementsByTagName(o)[0];
      a.async = 1;
      a.src = g;
      m.parentNode.insertBefore(a, m);
    })(window, document, "script", "https://www.google-analytics.com/analytics.js", "ga");
    ga("create", "UA-XXXXX-YY", "auto");
    ga("set", "checkProtocolTask", function() {});
    ga("require", "displayfeatures");
    ga("send", "pageview", "/mypage.html");
  },
  562: function(module, __webpack_exports__, __webpack_require__) {
    "use strict";
    __webpack_require__.r(__webpack_exports__);
    var browser_polyfill = __webpack_require__(19);
    var browser_polyfill_default = __webpack_require__.n(browser_polyfill);
    var webext_options_sync = __webpack_require__(30);
    var webext_options_sync_default = __webpack_require__.n(webext_options_sync);
    var semver_diff = __webpack_require__(334);
    var semver_diff_default = __webpack_require__.n(semver_diff);
    var storage = __webpack_require__(14);
    var changelogs = __webpack_require__(40);
    var settings = __webpack_require__(24);
    var ky = __webpack_require__(335);
    const BASE_URL = "https://api.faceit-enhancer.com";
    const api = ky["a"].extend({
      prefixUrl: BASE_URL
    });
    const fetchBans = () => api("bans").json();
    const fetchVips = () => api("vips").json();
    var background_api = api;
    var ga = __webpack_require__(559);
    storage["a"].define({
      defaults: settings["a"],
      migrations: [ savedOptions => {
        if (savedOptions.matchRoomAutoVetoMapItems && savedOptions.matchRoomAutoVetoMapItems.includes("de_cbble")) {
          savedOptions.matchRoomAutoVetoMapItems = savedOptions.matchRoomAutoVetoMapItems.filter(map => map !== "de_cbble");
          savedOptions.matchRoomAutoVetoMapItems.push("de_vertigo");
        }
        if (savedOptions.bans) {
          delete savedOptions.bans;
        }
        if (savedOptions.vips) {
          delete savedOptions.vips;
        }
      }, webext_options_sync_default.a.migrations.removeUnused ]
    });
    browser_polyfill_default.a.runtime.onMessage.addListener(async message => {
      if (!message) {
        return;
      }
      switch (message.action) {
       case "notification":
        {
          const {name: name} = browser_polyfill_default.a.runtime.getManifest();
          delete message.action;
          browser_polyfill_default.a.notifications.create("", {
            type: "basic",
            ...message,
            contextMessage: name,
            iconUrl: "icon.png"
          });
          break;
        }

       case "fetchApi":
        {
          try {
            const [bans, vips] = await Promise.all([ fetchBans(), fetchVips() ]);
            return {
              bans: bans,
              vips: vips
            };
          } catch (error) {
            console.error(error);
            return {
              bans: [],
              vips: []
            };
          }
        }

       default:
      }
    });
    browser_polyfill_default.a.runtime.onInstalled.addListener(async ({reason: reason, previousVersion: previousVersion}) => {
      if (reason === "update") {
        const {installType: installType} = await browser_polyfill_default.a.management.getSelf();
        if (installType === "development") {
          return;
        }
        const {version: version} = browser_polyfill_default.a.runtime.getManifest();
        const versionDiffType = semver_diff_default()(previousVersion, version);
        if (versionDiffType === null || versionDiffType === "patch") {
          return;
        }
        const changelogUrl = changelogs[version];
        if (changelogUrl) {
          const {updateNotificationType: updateNotificationType, updateNotifications: updateNotifications} = await storage["a"].getAll();
          switch (updateNotificationType) {
           case settings["d"][0]:
            {
              browser_polyfill_default.a.tabs.create({
                url: changelogUrl,
                active: false
              });
              break;
            }

           case settings["d"][1]:
            {
              updateNotifications.push(version);
              await storage["a"].set({
                updateNotifications: updateNotifications
              });
              browser_polyfill_default.a.browserAction.setBadgeText({
                text: updateNotifications.length.toString()
              });
              browser_polyfill_default.a.browserAction.setBadgeBackgroundColor({
                color: "#f50"
              });
              break;
            }

           default:
            {
              break;
            }
          }
        }
      }
    });
  }
});