#!/bin/bash

echo "Sync dist dir"

rsync -rtv "${PWD}/dist" /nfsshare/faceit-plugin
rsync -rtv "${PWD}/key.pem" /nfsshare/faceit-plugin/key.pem