#!/bin/bash

set -e

NAME="nadia-magnifier"

if [ ! -d "./dist" ]; then

    echo -e "ERROR! Dist DIR not exists! Run\n\n
    
    npm run build --env=production \n\n
    
FIRST!\n"

    exit 1
fi

rm -f $NAME.crx $NAME.zip
crx3 -o "./${NAME}.crx" -z "./${NAME}.zip" -p ./key.pem -x ./update.xml dist
zip -rj "./${NAME}.zip" dist/*
# rsync -rtv "./${NAME}.crx" "./${NAME}.zip" /nfsshare/faceit-plugin

echo "RELEASE SUCCESS!"