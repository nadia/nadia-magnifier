/* eslint-disable import/prefer-default-export */
import { getCurrentPath } from './location'

export const isRoomOverview = path =>
  /room\/.+-.+-.+-.+$/.test(path || getCurrentPath())

export const isPlayerProfileStats = path =>
  /players\/.+\/stats\//.test(path || getCurrentPath())

export const getHubPage = path => {
  path = path || getCurrentPath()
  const regexp = new RegExp(/hub\/([0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12})\//, "gmi")
  const m = regexp.exec(path) || []

  if (m.length == 2) {
    return m[1]
  }
  
  return false
}