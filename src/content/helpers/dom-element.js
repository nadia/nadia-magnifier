import select from 'select-dom'
export const ENHANCER_ATTRIBUTE = 'faceit-enhancer'

export const setFeatureAttribute = (featureName, element) =>
  element.setAttribute(`${ENHANCER_ATTRIBUTE}-${featureName}`, '')

export const hasFeatureAttribute = (featureName, element) =>
  element.hasAttribute(`${ENHANCER_ATTRIBUTE}-${featureName}`)

export const setStyle = (element, style) =>
  element.setAttribute(
    'style',
    typeof style === 'string' ? `${style}` : style.join(';')
  )

export const waitForElement = async (selector, callback) => {
  let element = await select(selector, document.body)
  
  if (!element) {
    setTimeout(() => {
      waitForElement(selector, callback)
    }, 200)

    return
  }

  callback(element)
}