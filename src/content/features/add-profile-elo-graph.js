/** @jsx h */
import { h } from 'dom-chef'
import select from 'select-dom'
import {
  getPlayer,
  getPlayerMatches,
} from '../helpers/faceit-api'
import { getPlayerProfileNickname } from '../helpers/player-profile'
import {
  hasFeatureAttribute,
  setFeatureAttribute
} from '../helpers/dom-element'
import createGraphElement from '../components/elo-graph'

const FEATURE_ATTRIBUTE = 'elo-graph'

export default async parentElement => {
  const performanceStatsElement = await select(
    '[ng-include="performanceTemplate"]',
    parentElement
  )
  if (!performanceStatsElement) {
    return
  }

  if (hasFeatureAttribute(FEATURE_ATTRIBUTE, performanceStatsElement)) {
    return
  }
  setFeatureAttribute(FEATURE_ATTRIBUTE, performanceStatsElement)

  const container = performanceStatsElement.parentElement.parentElement
  performanceStatsElement.style.display = 'none';

  if (!container) {
    return
  }

  const s = select('select', container)
  if (s) {
    s.remove()
  }
  
  
  const nickname = getPlayerProfileNickname()
  const player = await getPlayer(nickname)
  const matches = await getPlayerMatches(player.guid, 'csgo', 2000)

  let points = [];
  matches.forEach((match, i) => {
    points.push([match.createdAt, parseInt(match.elo)])
  })

  
  console.log(performanceStatsElement.parentElement)
  createGraphElement(performanceStatsElement.parentElement, player.nickname, points)
}
