/** @jsx h */
import { h } from 'dom-chef'
import select from 'select-dom'
import { getChatElement, getTeamSpeak } from '../helpers/match-room'
import {
  hasFeatureAttribute,
  setFeatureAttribute,
  setStyle
} from '../helpers/dom-element'

const FEATURE_ATTRIBUTE = 'teamspeak-channel'

export default async parent => {
  const ts3 = await getTeamSpeak(parent)
  const chatElement = await getChatElement(parent)

  if (!chatElement || !ts3) {
    return
  }

  const messagesBeginning = await select('div[id=BEGINNING]', chatElement)

  if (!messagesBeginning) {
    return
  }

  const chatMessages = await select.all(
    'div[class=paragraph]',
    messagesBeginning.parentElement
  )

  const tsChannelRgx = new RegExp('^.*?([0-9]{1,2})( ?)(A|B|a|b{1}).*?$', 'i')

  chatMessages.forEach(async messageElement => {
    if (
      !messageElement ||
      hasFeatureAttribute(FEATURE_ATTRIBUTE, messageElement)
    ) {
      return
    }

    if (!tsChannelRgx.test(messageElement.textContent)) {
      return
    }

    setFeatureAttribute(FEATURE_ATTRIBUTE, messageElement)

    const matches = tsChannelRgx.exec(messageElement.textContent)
    const channel = {
      number: parseInt(matches[1].trim()),
      faction: matches[3].trim().toUpperCase(),
      fullName: null
    }

    channel.fullName = `${channel.number}${channel.faction}`

    const path = ts3.channelPath
      .split('%channel_number%')
      .join(channel.number)
      .replace('%channel_full%', channel.fullName)
      .replace('%channel_faction%', channel.faction)

    const href = `ts3server://${ts3.address}/?channel=${encodeURIComponent(
      path
    )}`
    const title = `Połącz, TS - kanał ${channel.fullName}: ${ts3.address}`
    const TSIconElement = (
      <div>
        <div>{messageElement.textContent}</div>
        <a href={href} target="_blank" title={title} style={{  
          display: 'block',
          'border-top': '1px solid rgb(78, 78, 78)',
          padding: '10px 0 0 0',
          margin: '5px 0'
        }}>
          <img
            src="http://icons.iconarchive.com/icons/dakirby309/simply-styled/128/TeamSpeak-icon.png"
            alt="TeamSpeak3 Icon"
            width="25"
          />
          <span style={{ 'margin-left': '10px' }}>
            Dołącz na kanał: {channel.fullName}, {ts3.address}
          </span>
        </a>
      </div>
    )
    messageElement.textContent = ''
    messageElement.appendChild(TSIconElement)
  })
}
