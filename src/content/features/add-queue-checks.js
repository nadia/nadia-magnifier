/** @jsx h */
import { h } from 'dom-chef'
import {
    getHub,
    getHubQueue,
    getQueuePlayers,
    getPlayerStats
} from '../helpers/faceit-api'
import {
    hasFeatureAttribute,
    setFeatureAttribute,
    waitForElement
} from '../helpers/dom-element'
import select from 'select-dom'
import {
    queueAvgElement, 
    QUEUE_STATE,
    QUEUE_COLORS,
    queueColorLegendElement,
    QUEUE_STATE_TRANS
} from '../components/queue'

import createPlayerStatsElement from '../components/player-stats'

const FEATURE_ATTRIBUTE = 'queue-checks'

export default async (parent, hubID) => {
    const queuingCounter = select('.queuing-counter', parent)

    if (!queuingCounter || hasFeatureAttribute(FEATURE_ATTRIBUTE, queuingCounter)) {
        return
    }
    setFeatureAttribute(FEATURE_ATTRIBUTE, queuingCounter)

    const queue = await getHubQueue(hubID)
    const hub = await getHub(hubID)

    if (!queue) {
        return
    }

    queuingCounter.addEventListener('mouseenter', async (event) => {
        let players = await getQueuePlayers(queue.id) || []
        
        let popover = waitForElement('ul.users-list', usersListElement => {
            const items = select.all('.users-list__item', usersListElement) || []
            
            usersListElement.style['max-height'] = '500px'
            usersListElement.style.overflow = 'auto'

            items.forEach(async (item, index) => {

                if (hasFeatureAttribute(FEATURE_ATTRIBUTE, item)) {
                    return
                }

                setFeatureAttribute(FEATURE_ATTRIBUTE, item)

                const playerElement = select('strong', item)
                const nickname = playerElement.textContent
                
                const profileLinkElement = (
                    <a href={`/pl/players-modal/${playerElement.textContent}`}
                        target="_blank">
                        {playerElement.textContent}
                    </a>
                )

                playerElement.textContent = ''
                playerElement.appendChild(profileLinkElement)

                const player = players[index]
                let stats = await getPlayerStats(player.userId, 'csgo', 20, {
                    id: hubID,
                    name: hub.name
                })

                const statsElement = createPlayerStatsElement(stats)

                item.style.width = '265px'
                item.style['flex-wrap'] = 'wrap'
                item.style.border = '0'
                item.parentNode.style['border-bottom'] = '1px solid rgb(78,78,78)'
                item.parentNode.appendChild(statsElement)
            })
        })
    })

    const setQueueState = (state) => {
        if (state == QUEUE_STATE.EMPTY) {
            queuingCounter.style.background = 'none'
        } else {
            queuingCounter.parentElement.style.height = '100%'
            queuingCounter.style.height = '100%'
            queuingCounter.style.background = QUEUE_COLORS[state]
            queuingCounter.style.padding = '0 10px'
        }

        const items = select.all('.queuing-counter__item', queuingCounter) || []

        items.forEach(item => {
            item.style.color = 'white'
        })

        queuingCounter.style.cursor = 'pointer'
        queuingCounter.setAttribute('title', QUEUE_STATE_TRANS[state])
    }

    const queueCounterElement = select('div.queuing-counter__item:last-child span', queuingCounter)

    const qState = async () => {
        let nums = {
            lowestAvg: 100,
            totalAvgKills: 0,
            queueAvgKills: 0,
        }

        let players = await getQueuePlayers(queue.id) || []
        queueCounterElement.textContent = players.length

        if (!players.length) {
            nums.queueAvgKills = 0
            nums.lowestAvg = 100
            setQueueState(QUEUE_STATE.EMPTY)
        }

        await Promise.all(players.map(async player => {
            let stats = await getPlayerStats(player.userId, 'csgo', 20, {
                id: hubID,
                name: hub.name
            })
            if (!stats) {
                return
            }
    
            let avg  = parseInt(stats.averageKills)
            nums.totalAvgKills += avg
            if (avg < nums.lowestAvg) {
                nums.lowestAvg = avg
            }
        }))

        if (nums.lowestAvg <= 7) {
            setQueueState(QUEUE_STATE.BAD)
        } else if (nums.lowestAvg <= 13) {
            setQueueState(QUEUE_STATE.WARNING)
        } else if (nums.lowestAvg > 13) {
            setQueueState(QUEUE_STATE.OK)
        } else {
            setQueueState(QUEUE_STATE.EMPTY)
        } 

        let avgKillsElement = select('div.avgKills', queuingCounter)
        let queueAvgKills = 0

        if (players.length)
            queueAvgKills = Math.round(nums.totalAvgKills / players.length * 100) / 100

        if (nums.queueAvgKills != queueAvgKills) {    
            if (!avgKillsElement) {
                queuingCounter.appendChild(queueAvgElement(queueAvgKills))
            } else {
                const spanAvgElement = select('span.avg', avgKillsElement)
                spanAvgElement.textContent = queueAvgKills

                avgKillsElement.previousElementSibling
            }

            nums.queueAvgKills = queueAvgKills
        }
    }

    qState()
    setInterval(qState, 10*1000)
}