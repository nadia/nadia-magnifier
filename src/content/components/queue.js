/** @jsx h */
import { h } from 'dom-chef'

export const queueAvgElement = (avgValue) => (
    <div class="avgKills queuing-counter__item" style={{color: 'white'}}>
        <span>Avg: </span>
        <span class="avg">{avgValue}</span>
    </div>
)

export const QUEUE_STATE = {
    OK: 'ok',
    WARNING: 'warn',
    BAD: 'bad',
    EMPTY: 'none'
}

export const QUEUE_COLORS = {
    [QUEUE_STATE.BAD]: 'red',
    [QUEUE_STATE.WARNING]: 'orange',
    [QUEUE_STATE.OK]: 'green',
    [QUEUE_STATE.EMPTY]: 'none'
}

export const QUEUE_STATE_TRANS = {
    [QUEUE_STATE.BAD]: 'Gracz z średnią zabójstw mniejszą lub równą 7',
    [QUEUE_STATE.WARNING]: 'Gracz z średnią zabójstw mniejszą lub równą 13',
    [QUEUE_STATE.OK]: 'Wszyscy gracze z średnią zabójstw powyżej 13',
    [QUEUE_STATE.EMPTY]: 'Brak graczy w kolejce'
}