/** @jsx h */
import { h } from 'dom-chef'
import {getImaegForPoints} from '../helpers/svg'

const stat = (value, label, flex = 1) => (
  <div style={{ flex, padding: '5px 9px' }}>
    <span dangerouslySetInnerHTML={{__html: value}}></span>
    <div className="text-sm">{label}</div>
  </div>
)

const statsVerticalDivider = () => (
  <div style={{ width: 1, background: '#333' }} />
)

export default ({
  matches,
  winRate,
  averageKDRatio,
  averageKills,
  averageKRRatio,
  today,
  entity,
  createdAt,
  competition,
  averageHeadshots,
  alignRight = false
}) => {
  let todayDisplay = 'block'
  let padding = '0 0 0 57px'

  if (Array.isArray(today.winRate))
  today.winRate = today.winRate[0] == 'win' ? 100 : 0

  if (competition.today && competition.today.matches) {
    competition.today.winRate = competition.today.winRate[0] == 'win' ? 100 : 0
  }

  if (competition.last20 && competition.last20.matches) {
    competition.last20.winRate = competition.last20.winRate[0] == 'win' ? 100 : 0
  }

  if (!today.averageKills) {
    todayDisplay = 'none'
    padding = ''
  }

  let sources = {
    today,
    last20: {matches, winRate, averageKills, averageHeadshots, averageKDRatio, averageKRRatio}
  }

  const hubValue = (val, color = "#777") => {
    return ( <span style={{color}}>{val}</span> )
  }

  const getSectionTitle = (period = 'last20', options = {color: 'cyan'}) => {

    var hubPrefix = ''
    var hubSuffix = ''

    if (competition[period].matches) {
      hubPrefix = (
        <span>{hubValue(competition[period].matches, options.color)} / </span>
      )

      hubSuffix = (
      <span>{hubValue(`${competition[period].matches}`, options.color)} in {hubValue(entity.name || '?', options.color)}</span>
      )
    }

    let title = (
      <span>Last 20 matches<br/>{hubSuffix}</span>
    )

    if (period == 'today') {
      title = (
        <span>Today ( {hubPrefix} {sources[period].matches} )</span>
      )
    }

    return title
  }

  const getStat = (key, label = 'Test label', period = 'last20', suffix = '') => {

    let value
    let comp = ''

    if (key == 'averageKills') {
      if (competition[period][key]) {
        comp = `<span style="color: cyan">${competition[period][key]}&nbsp;/&nbsp;${competition[period]['averageHeadshots']}${suffix}</span><br/>`
      }
      value = `${sources[period][key]}&nbsp;/&nbsp;${sources[period]['averageHeadshots']}${suffix}`
    } else if ( key == 'averageKDRatio' ) {
      if (competition[period][key]) {
        comp = `<span style="color: cyan">${competition[period][key]}&nbsp;/&nbsp;${competition[period]['averageKRRatio']}</span><br/>`
      }
      value = `${sources[period][key]}&nbsp;/&nbsp;${sources[period]['averageKRRatio']}`
    } else {
      if (competition[period][key]) {
        value = `<span style="color: cyan">${competition[period][key]}${suffix}</span>&nbsp;/&nbsp;${sources[period][key]}${suffix}`
      } else {
        value = `${sources[period][key]}${suffix}`
      }
    }

    return stat(`${comp}${value}`, label, 1, 20)
  }

  const created = new Date(createdAt)

  const diffDays = toDate => {
    const now = new Date()
    return Math.round((now.getTime()-toDate.getTime())/(1000*60*60*24));
  }

  return (
  <div
    className="text-muted flex-2"
    style={{
      display: 'flex',
      'border-top': '1px solid #333',
      'text-align': alignRight && 'right',
      'font-size': 12,
      'line-height': 12,
      'flex-flow': 'wrap',
      'width': '325px',
    }}
  >
    {/* <div style={{padding: '5px 10px'}}>
      {getImaegForPoints('test', [])}
    </div> */}
    <div>
      <div
        className="text-sm"
        style={{ 'border-bottom': '1px solid #333', padding: '5px 9px' }}
      >
        Overall
      </div>
      
      <div style={{'font-size': '10px', padding: '0 9px'}}>
        <div style={{margin: '6px 0'}}>Matches <br/><span style={{color: 'yellow'}}>{matches}</span></div>
        <div>Created</div>
        <div style={{'margin-bottom': '6px'}}>
          <span style={{color: 'yellow'}}>{created.toLocaleDateString("pl-PL")}</span>
          <br/>
          <span style={{color: 'yellow'}}>{diffDays(created)}</span> days ago
        </div>
      </div>
    </div>
    {statsVerticalDivider()}
    <div style={{flex: 4}}>
      <div style={{ flex: 4, display: todayDisplay }}>
        <div
          className="text-sm"
          style={{ 'border-bottom': '1px solid #333', padding: '5px 9px' }}
        >
        {getSectionTitle('today')}
        </div>
        <div style={{ display: 'flex' }}>
          {getStat('winRate', 'Win Rate', 'today', '%')}
          {statsVerticalDivider()}
          {getStat('averageKills', 'Avg. Kills / HS %', 'today', '%')}
          {statsVerticalDivider()}
          {getStat('averageKDRatio', 'Avg. K/D / K/R', 'today')}
        </div>
      </div>
      <div style={{ flex: 4 , display: 'block'}}>
        <div
          className="text-sm"
          style={{  'border-top': '1px solid #333', 'border-bottom': '1px solid #333', padding: '5px 9px' }}
        >
          {getSectionTitle('last20')}
        </div>
        <div style={{ display: 'flex' }}>
          {getStat('winRate', 'Win Rate', 'last20', '%')}
          {statsVerticalDivider()}
          {getStat('averageKills', 'Avg. Kills / HS %', 'last20', '%')}
          {statsVerticalDivider()}
          {getStat('averageKDRatio', 'Avg. K/D / K/R')}
        </div>
      </div>
    </div>
  </div>
)}
