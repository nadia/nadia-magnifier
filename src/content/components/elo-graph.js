/** @jsx h */
import { h } from 'dom-chef'
import Highcharts from '../helpers/highcharts'
// Load the exporting module.
import Exporting from 'highcharts/modules/exporting';
// Initialize exporting module.
Exporting(Highcharts);

export default (element, nickname, points) => {
    
    Highcharts.chart(element,  {

        chart: {
            defaultSeriesType: 'line'
        },

        title: {
            text: 'ELO Rating Progression'
        },
        navigator: {
            enabled: true
        },
        scrollbar: {
            enabled: true
        },
        rangeSelector: {
            enabled: true
        },

        yAxis: {
            title: {
                text: 'ELO'
            }
        },
    
        xAxis: {
            type: 'datetime',
        },
    
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },
    
        plotOptions: {
            line: {
                marker: {
                    enabled: false,
                    radius: 2
                },
                states: {
                    hover: {
                        lineWidth: 2
                    }
                }
            },
        },
    
        series: [{
            name: nickname,
            data: points
        }],
    
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 500
                },
                chartOptions: {
                    legend: {
                        layout: 'horizontal',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }
    
    });
}